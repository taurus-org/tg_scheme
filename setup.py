#!/usr/bin/env python

##############################################################################
##
# This file is part of Taurus
##
# http://taurus-scada.org
##
# Copyright 2011 CELLS / ALBA Synchrotron, Bellaterra, Spain
##
# Taurus is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
##
# Taurus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
##
# You should have received a copy of the GNU Lesser General Public License
# along with Taurus.  If not, see <http://www.gnu.org/licenses/>.
##
##############################################################################

from setuptools import setup, find_packages

version = '0.0.1'
description = 'Simplified Tango Taurus scheme'
long_description = '''tg is a Taurus scheme for accessing a tango controls system.
It is a reimplementation aimed at simplifying the code and the behaviour compared
to the original 'tango' scheme. It also aims to be as asynchronous as possible
during subscriptions and reads
'''
license = 'LGPL'
platforms = ['Linux', 'Windows']


authors = 'cpascual'
email = 'cpascual@cells.es'
url = 'https://gitlab.com/taurus-org/tg_scheme'

install_requires = ['taurus>=5',
                    'pytango>=9.3.3']

entry_points = {
    "taurus.core.schemes": ["taurus_tg_scheme = taurus_tg_scheme",],
}


setup(name='taurus_tg_scheme',
      version=version,
      description=description,
      long_description=long_description,
      author=authors,
      maintainer=authors,
      maintainer_email=email,
      url=url,
      platforms=platforms,
      license=license,
      packages=find_packages(),
      include_package_data=True,
      install_requires=install_requires,
      entry_points=entry_points,
      )

