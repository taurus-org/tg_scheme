# tg_scheme

A (WIP) reimplementation of the Tango Taurus scheme as a plugin

## Installation

`pip install tg_scheme`

## Usage

Use its attributes just as you would for original tango attributes in Taurus. 
Just use `tg:` instead of `tango:` for the scheme, e.g.:

```console
> taurus form tg:sys/tg_test/1/ampli 
```
### Hint:

You can set `tg` as the default scheme using in your settings file 
```
[taurus]
DEFAULT_SCHEME = "tg"
```

or, programmatically, with:
```python
import taurus.tauruscustomsettings

taurus.tauruscustomsettings.DEFAULT_SCHEME = "tg"
```

